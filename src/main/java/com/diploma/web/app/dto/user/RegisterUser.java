package com.diploma.web.app.dto.user;

import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;

public class RegisterUser {

    private String email;
    private Role role;
    private Map<String, Object> data;
    private char[] password;


    public RegisterUser() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(@Nonnull String email) {
        this.email = Preconditions.checkNotNull(email);
    }

    public Role getRole() {
        return role;
    }

    public void setRole(@Nullable Role role) {
        this.role = role;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(@Nonnull Map<String, Object> data) {
        this.data = Preconditions.checkNotNull(data);
    }

    public char[] getPassword() {
        return password;
    }

    public void setPassword(@Nonnull char[] password) {
        this.password = Preconditions.checkNotNull(password);
    }

}

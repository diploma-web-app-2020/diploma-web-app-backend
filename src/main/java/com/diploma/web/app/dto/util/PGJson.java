package com.diploma.web.app.dto.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.postgresql.util.PGobject;

import java.io.Serializable;
import java.util.Map;

public class PGJson<K, V> extends PGobject implements Serializable {

    private PGJson(Map<K, V> map) {
        setType("json");
        setValue(new ObjectMapper().convertValue(map, JsonNode.class).toString());
    }

    public static <K, V> PGJson<K, V> of(Map<K, V> map) {
        return new PGJson<>(map);
    }

    @Override
    public @Nullable String getValue() {
        return value;
    }

    @Override
    public void setValue(@Nullable String value) {
        this.value = value;
    }

}

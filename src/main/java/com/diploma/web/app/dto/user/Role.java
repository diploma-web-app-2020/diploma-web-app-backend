package com.diploma.web.app.dto.user;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    USER,
    ORGANIZATION;

    @Override
    public String getAuthority() {
        return toString();
    }

}

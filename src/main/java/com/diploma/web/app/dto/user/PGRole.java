package com.diploma.web.app.dto.user;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.postgresql.util.PGobject;

import java.io.Serializable;

public class PGRole extends PGobject implements Serializable {

    private PGRole(Role role) {
        setType("role");
        setValue(role.toString());
    }

    public static PGRole of(Role role) {
        return new PGRole(role);
    }

    @Override
    public @Nullable String getValue() {
        return value;
    }

    @Override
    public void setValue(@Nullable String value) {
        this.value = value;
    }

}

package com.diploma.web.app.dto.auth;

import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;

public class Session {

    @Nonnull
    private final String token;
    @Nonnull
    private final Long expiration;
    @Nonnull
    private final Long userId;

    public Session(@Nonnull String token,
                   @Nonnull Long expiration,
                   @Nonnull Long userId) {
        this.token = Preconditions.checkNotNull(token);
        this.expiration = Preconditions.checkNotNull(expiration);
        this.userId = Preconditions.checkNotNull(userId);
    }

    @Nonnull
    public String getToken() {
        return token;
    }

    @Nonnull
    public Long getExpiration() {
        return expiration;
    }

    @Nonnull
    public Long getUserId() {
        return userId;
    }

}

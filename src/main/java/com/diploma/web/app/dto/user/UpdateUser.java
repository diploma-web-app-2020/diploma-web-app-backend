package com.diploma.web.app.dto.user;

import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import java.util.Map;

public class UpdateUser {

    private Map<String, Object> data;

    public UpdateUser() {
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(@Nonnull Map<String, Object> data) {
        this.data = Preconditions.checkNotNull(data);
    }

}

package com.diploma.web.app.dao.api;

import com.diploma.web.app.dto.auth.Session;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Optional;

public interface UserSessionDao {

    /**
     * Loads session by token.
     *
     * @param token of the requested session
     * @return {@link Session} wrapped in {@link Optional}. Empty if no such session is available.
     */
    @Nonnull
    Optional<Session> loadSession(@Nonnull String token);

    /**
     * Loads all current session in the database.
     *
     * @return {@link List<Session>} of all the sessions in the database.
     */
    @Nonnull
    List<Session> loadAllSessions();

    /**
     * Create a new session with the given dto.
     *
     * @param session to create new session with
     */
    void createSession(@Nonnull Session session);

    /**
     * Delete the session with the given token.
     *
     * @param token requested session token
     */
    void deleteSession(@Nonnull String token);

    /**
     * Delete all the sessions of the requested user.
     *
     * @param userId of the requested user
     * @return {@link List<String>} of all the deleted session tokens.
     */
    @Nonnull
    List<String> deleteUserSessions(@Nonnull Long userId);

}

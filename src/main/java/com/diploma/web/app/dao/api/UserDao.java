package com.diploma.web.app.dao.api;

import com.diploma.web.app.dto.user.User;

import javax.annotation.Nonnull;
import java.util.Map;
import java.util.Optional;

public interface UserDao {

    /**
     * Loads user by his id.
     *
     * @param id of the requested user
     * @return {@link User} wrapped in {@link Optional}. Empty if no such user is available.
     */
    @Nonnull
    Optional<User> loadUserById(@Nonnull Long id);

    /**
     * Loads user by his id.
     *
     * @param email of the requested user
     * @return {@link User} wrapped in {@link Optional}. Empty if no such user is available.
     */
    @Nonnull
    Optional<User> loadUserByEmail(@Nonnull String email);

    /**
     * Checks if a user with the given email exists.
     *
     * @param email of the requested user
     * @return {@code true} if user exists and {@code false} otherwise.
     */
    boolean isExistingUserEmail(@Nonnull String email);

    /**
     * Creates a new user.
     *
     * @param user to be created
     * @return id of the newly created user.
     */
    @Nonnull
    Long createUser(@Nonnull User user);

    /**
     * Updates data of the user with the given id.
     *
     * @param id   of the requested user
     * @param data updated data of the user
     */
    void updateUserData(@Nonnull Long id,
                        @Nonnull Map<String, Object> data);

}

package com.diploma.web.app.dao.impl;

import com.diploma.web.app.dao.api.UserSessionDao;
import com.diploma.web.app.dto.auth.Session;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.annotation.Nonnull;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@SuppressWarnings("ConstantConditions")
public class UserSessionDaoImpl implements UserSessionDao {

    @Nonnull
    private final NamedParameterJdbcTemplate jdbcTemplate;

    public UserSessionDaoImpl(@Nonnull NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = Preconditions.checkNotNull(jdbcTemplate);
    }

    @Nonnull
    @Override
    public Optional<Session> loadSession(@Nonnull String token) {
        try {
            return Optional.of(
                    jdbcTemplate.queryForObject(
                            "select * from user_sessions where token = :token limit 1",
                            Collections.singletonMap("token", token),
                            this::sessionRowMapper
                    )
            );
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Nonnull
    @Override
    public List<Session> loadAllSessions() {
        try {
            return jdbcTemplate.query(
                    "select * from user_sessions",
                    this::sessionRowMapper
            );
        } catch (EmptyResultDataAccessException e) {
            return Collections.emptyList();
        }
    }

    @Override
    public void createSession(@Nonnull Session session) {
        jdbcTemplate.update(
                "insert into user_sessions (user_id, token, expiration) values (:userId, :token, :expiration)",
                ImmutableMap.of(
                        "userId", session.getUserId(),
                        "token", session.getToken(),
                        "expiration", new Timestamp(session.getExpiration())
                )
        );
    }

    @Override
    public void deleteSession(@Nonnull String token) {
        jdbcTemplate.update(
                "delete from user_sessions where token = :token",
                Collections.singletonMap("token", token)
        );
    }

    @Nonnull
    @Override
    public List<String> deleteUserSessions(@Nonnull Long userId) {
        KeyHolder tokensHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(
                "delete from user_sessions where user_id = :userId returning token",
                new MapSqlParameterSource().addValue("userId", userId),
                tokensHolder
        );
        return tokensHolder
                .getKeyList()
                .stream()
                .map(tokenRow -> (String) tokenRow.get("token"))
                .collect(Collectors.toList());
    }

    private Session sessionRowMapper(ResultSet rs, int rowNum) {
        try {
            return new Session(
                    rs.getString("token"),
                    rs.getTimestamp("expiration").getTime(),
                    rs.getLong("user_id")
            );
        } catch (SQLException e) {
            throw new RuntimeException("Error while reading user data from db", e);
        }
    }

}

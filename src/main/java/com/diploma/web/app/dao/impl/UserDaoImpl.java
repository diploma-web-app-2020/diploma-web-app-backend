package com.diploma.web.app.dao.impl;

import com.diploma.web.app.dao.api.UserDao;
import com.diploma.web.app.dto.user.PGRole;
import com.diploma.web.app.dto.user.Role;
import com.diploma.web.app.dto.user.User;
import com.diploma.web.app.dto.util.PGJson;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.annotation.Nonnull;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;

@SuppressWarnings("ConstantConditions")
public class UserDaoImpl implements UserDao {

    @Nonnull
    private final NamedParameterJdbcTemplate jdbcTemplate;

    public UserDaoImpl(@Nonnull NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = Preconditions.checkNotNull(jdbcTemplate);
    }

    @Nonnull
    @Override
    public Optional<User> loadUserById(@Nonnull Long id) {
        try {
            return Optional.of(
                    jdbcTemplate.queryForObject(
                            "select * from users where id = :id",
                            Collections.singletonMap("id", id),
                            this::userRowMapper
                    )
            );
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Nonnull
    @Override
    public Optional<User> loadUserByEmail(@Nonnull String email) {
        try {
            return Optional.of(
                    jdbcTemplate.queryForObject(
                            "select * from users where email = :email",
                            Collections.singletonMap("email", email),
                            this::userRowMapper
                    )
            );
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public boolean isExistingUserEmail(@Nonnull String email) {
        try {
            // If this didn't throw exception, then there is exactly one user with the given email
            jdbcTemplate.queryForMap(
                    "select email from users where email = :email",
                    Collections.singletonMap("email", email)
            );
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }

    @Nonnull
    @Override
    public Long createUser(@Nonnull User user) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(
                "insert into users (email, role, is_blocked, data, hashed_password, salt) values (:email, :role, :isBlocked, :data, :hashedPassword, :salt) returning id",
                new MapSqlParameterSource()
                        .addValue("email", user.getEmail())
                        .addValue("role", PGRole.of(user.getRole()))
                        .addValue("isBlocked", user.isBlocked())
                        .addValue("data", PGJson.of(user.getData()))
                        .addValue("hashedPassword", user.getPassword())
                        .addValue("salt", user.getSalt()),
                keyHolder
        );
        return keyHolder.getKeyAs(Long.class);
    }

    @Override
    public void updateUserData(@Nonnull Long id,
                               @Nonnull Map<String, Object> data) {
        jdbcTemplate.update(
                "update users set data = :data where id = :id",
                ImmutableMap.of(
                        "data", PGJson.of(data),
                        "id", id
                )
        );
    }

    @Nonnull
    private User userRowMapper(ResultSet rs, int rowNum) {
        try {
            return new User(
                    rs.getLong("id"),
                    rs.getString("email"),
                    Role.valueOf(rs.getString("role")),
                    new ObjectMapper().readValue(rs.getString("data"), new TypeReference<>() {
                    }),
                    rs.getBoolean("is_blocked"),
                    rs.getString("hashed_password"),
                    rs.getString("salt")
            );
        } catch (SQLException | JsonProcessingException e) {
            throw new RuntimeException("Error while reading user data from db", e);
        }
    }

}

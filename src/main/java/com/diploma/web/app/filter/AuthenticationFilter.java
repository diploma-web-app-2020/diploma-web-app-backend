package com.diploma.web.app.filter;


import com.diploma.web.app.service.api.JWTTokenService;
import com.diploma.web.app.service.api.UserService;
import com.diploma.web.app.service.api.UserSessionService;
import com.diploma.web.app.utils.exception.ExceptionResponse;
import com.diploma.web.app.utils.exception.UnauthorizedException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.annotation.Nonnull;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

public class AuthenticationFilter extends OncePerRequestFilter {

    @Nonnull
    private final UserService userService;
    @Nonnull
    private final UserSessionService userSessionService;
    @Nonnull
    private final JWTTokenService jwtTokenService;

    public AuthenticationFilter(@Nonnull UserService userService,
                                @Nonnull UserSessionService userSessionService,
                                @Nonnull JWTTokenService jwtTokenService) {
        this.userService = Preconditions.checkNotNull(userService, "UserDetailsService should not be null");
        this.userSessionService = Preconditions.checkNotNull(userSessionService, "UserSessionService should not be null");
        this.jwtTokenService = Preconditions.checkNotNull(jwtTokenService, "JWTTokenService should not be null");
    }


    @Override
    protected void doFilterInternal(@Nonnull HttpServletRequest request,
                                    @Nonnull HttpServletResponse response,
                                    @Nonnull FilterChain filterChain) throws IOException {
        try {
            String jwtToken = extractJwt(request);
            if (jwtToken != null) {
                userSessionService.checkSession(jwtToken);
                UserDetails userDetails = userService.loadUserByEmail(extractEmailFromJwt(jwtToken));
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                        userDetails, jwtToken, userDetails.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
            filterChain.doFilter(request, response);
        } catch (Exception e) {
            response.setStatus(401);
            response
                    .getWriter()
                    .write(convertObjectToJson(ExceptionResponse.of(new UnauthorizedException(), "Unauthorized")));
        }
    }

    private String extractJwt(HttpServletRequest request) {
        String headerAuth = request.getHeader("Authorization");
        if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
            return headerAuth.substring(7);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    private String extractEmailFromJwt(String jwt) {
        return (String) ((Map<String, Object>) jwtTokenService.readJWTToken(jwt).getBody()).get("email");
    }

    private String convertObjectToJson(Object object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            return "";
        }
    }

}

package com.diploma.web.app.configuration;

import com.diploma.web.app.utils.exception.ExceptionResponse;
import com.diploma.web.app.utils.exception.InvalidUserDataException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpStatusCodeException;

@ControllerAdvice
public class ExceptionHandlersConfiguration {

    @ExceptionHandler(HttpStatusCodeException.class)
    public ResponseEntity<ExceptionResponse> handleException(HttpStatusCodeException exception) {
        return prepareResponse(exception, exception.getMessage(), exception.getStatusCode());
    }

    @ExceptionHandler(InvalidUserDataException.class)
    public ResponseEntity<ExceptionResponse> handleException(InvalidUserDataException exception) {
        return prepareResponse(exception, "User data is invalid", exception.getStatusCode());
    }

    private ResponseEntity<ExceptionResponse> prepareResponse(Throwable exception,
                                                              String error,
                                                              HttpStatus httpStatus) {
        return new ResponseEntity<>(
                new ExceptionResponse(httpStatus.value(), error, exception.getMessage()),
                httpStatus
        );
    }

}

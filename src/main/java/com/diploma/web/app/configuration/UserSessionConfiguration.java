package com.diploma.web.app.configuration;

import com.diploma.web.app.dao.api.UserSessionDao;
import com.diploma.web.app.dao.impl.UserSessionDaoImpl;
import com.diploma.web.app.service.api.JWTTokenService;
import com.diploma.web.app.service.api.UserService;
import com.diploma.web.app.service.api.UserSessionService;
import com.diploma.web.app.service.impl.JWTTokenServiceImpl;
import com.diploma.web.app.service.impl.UserSessionServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Configuration
public class UserSessionConfiguration {

    private static final String PRIVATE_KEY_PATH = "private-key.der";

    @Bean
    public UserSessionDao userSessionDao(NamedParameterJdbcTemplate jdbcTemplate) {
        return new UserSessionDaoImpl(jdbcTemplate);
    }

    @Bean
    public UserSessionService userSessionService(UserService userService,
                                                 UserSessionDao userSessionDao,
                                                 JWTTokenService jwtTokenService,
                                                 @Value("${users.session.validityInMinutes}") Long sessionValidityInMinutes,
                                                 @Value("${users.session.tokenCacheValidityInSeconds}") Long tokenCacheValidityInSeconds,
                                                 @Value("${users.session.tokenRemovalDelayInMinutes}") Long tokenRemovalDelayInMinutes) {
        return new UserSessionServiceImpl(
                userService,
                userSessionDao,
                jwtTokenService,
                sessionValidityInMinutes,
                tokenCacheValidityInSeconds,
                tokenRemovalDelayInMinutes
        );
    }

    @Bean
    public JWTTokenService jwtTokenService() {
        try {
            return new JWTTokenServiceImpl(Files.readAllBytes(Paths.get(PRIVATE_KEY_PATH)));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

}

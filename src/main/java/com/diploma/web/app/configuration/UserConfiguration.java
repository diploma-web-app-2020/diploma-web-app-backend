package com.diploma.web.app.configuration;

import com.diploma.web.app.dao.api.UserDao;
import com.diploma.web.app.dao.impl.UserDaoImpl;
import com.diploma.web.app.service.api.UserDataValidator;
import com.diploma.web.app.service.api.UserService;
import com.diploma.web.app.service.impl.UserDataValidatorImpl;
import com.diploma.web.app.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

@Configuration
public class UserConfiguration {

    @Bean
    public UserDao userDao(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        return new UserDaoImpl(namedParameterJdbcTemplate);
    }

    @Bean
    public UserService userService(UserDao userDao,
                                   UserDataValidator userDataValidator) {
        return new UserServiceImpl(userDao, userDataValidator);
    }

    @Bean
    public UserDataValidator userDataValidator(@Value("${users.validator.minPasswordLength}") int minPasswordLength,
                                               @Value("${users.validator.maxPasswordLength}") int maxPasswordLength,
                                               @Value("${users.validator.lowerAndUpperCaseCheck}") boolean lowerAndUpperCaseCheck,
                                               @Value("${users.validator.numberCheck}") boolean numberCheck,
                                               @Value("${users.validator.specialCharacterCheck}") boolean specialCharacterCheck,
                                               UserDao userDao) {
        return new UserDataValidatorImpl(minPasswordLength, maxPasswordLength, lowerAndUpperCaseCheck, numberCheck, specialCharacterCheck, userDao);
    }

}

package com.diploma.web.app.configuration;

import com.diploma.web.app.filter.AuthenticationFilter;
import com.diploma.web.app.service.api.JWTTokenService;
import com.diploma.web.app.service.api.UserService;
import com.diploma.web.app.service.api.UserSessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.annotation.Nonnull;

@Configuration
@EnableWebSecurity
public class AuthenticationConfiguration extends WebSecurityConfigurerAdapter {

    @Nonnull
    private final UserService userService;
    @Nonnull
    private final UserSessionService userSessionService;
    @Nonnull
    private final JWTTokenService jwtTokenService;

    @Autowired
    public AuthenticationConfiguration(@Nonnull UserService userService,
                                       @Nonnull UserSessionService userSessionService,
                                       @Nonnull JWTTokenService jwtTokenService) {
        this.userService = userService;
        this.userSessionService = userSessionService;
        this.jwtTokenService = jwtTokenService;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors()
                .and()
                .csrf().disable()
                .logout().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/login", "/user/USER").permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilterBefore(
                        new AuthenticationFilter(userService, userSessionService, jwtTokenService),
                        UsernamePasswordAuthenticationFilter.class
                );
    }

}

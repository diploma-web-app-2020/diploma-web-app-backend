package com.diploma.web.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource(value = "classpath:service.properties")
public class DiplomaWebAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(DiplomaWebAppApplication.class, args);
    }

}

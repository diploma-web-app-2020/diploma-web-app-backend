package com.diploma.web.app.utils.exception;

import org.springframework.web.client.HttpStatusCodeException;

public class ExceptionResponse {

    private final int status;
    private final String error;
    private final String message;

    public ExceptionResponse(int status, String error, String message) {
        this.status = status;
        this.error = error;
        this.message = message;
    }

    public static ExceptionResponse of(HttpStatusCodeException exception) {
        return new ExceptionResponse(exception.getStatusCode().value(), exception.getMessage(), exception.getMessage());
    }

    public static ExceptionResponse of(HttpStatusCodeException exception, String error) {
        return new ExceptionResponse(exception.getStatusCode().value(), error, exception.getMessage());
    }

    public String getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public int getStatus() {
        return status;
    }

}

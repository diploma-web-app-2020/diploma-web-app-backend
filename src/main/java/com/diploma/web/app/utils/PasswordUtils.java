package com.diploma.web.app.utils;

import com.google.common.hash.Hashing;

import javax.annotation.Nonnull;
import java.nio.charset.StandardCharsets;

public class PasswordUtils {

    @SuppressWarnings("UnstableApiUsage")
    public static String hashPassword(@Nonnull char[] password,
                                      @Nonnull String salt) {
        return Hashing
                .sha256()
                .hashString(String.valueOf(password) + salt, StandardCharsets.UTF_8)
                .toString();
    }

}

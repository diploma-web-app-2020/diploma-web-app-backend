package com.diploma.web.app.utils.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;

public class InvalidUserDataException extends HttpStatusCodeException {

    public InvalidUserDataException(String message) {
        super(message, HttpStatus.UNPROCESSABLE_ENTITY, "Invalid user data", null, null, null);
    }

}

package com.diploma.web.app.utils.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;

public class UnauthorizedException extends HttpStatusCodeException {

    public UnauthorizedException() {
        super("Email, password or the combination is incorrect!", HttpStatus.UNAUTHORIZED, "Unauthorized", null, null, null);
    }

}

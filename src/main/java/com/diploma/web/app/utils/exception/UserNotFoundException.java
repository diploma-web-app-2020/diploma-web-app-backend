package com.diploma.web.app.utils.exception;

public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException(String userEmail) {
        super(String.format("User with %s email was not found", userEmail));
    }

    public UserNotFoundException(Long userId) {
        super(String.format("User with %d id was not found", userId));
    }

}

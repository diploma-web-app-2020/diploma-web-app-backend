package com.diploma.web.app.utils;

import com.diploma.web.app.dto.user.User;
import org.springframework.security.core.context.SecurityContextHolder;

public class AuthUtils {

    public static User getCurrentUser() {
        return (User) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();
    }

    public static Long getCurrentUserId() {
        return getCurrentUser().getId();
    }

    public static String getCurrentUserEmail() {
        return getCurrentUser().getEmail();
    }

    public static String getCurrentUserToken() {
        return (String) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getCredentials();
    }

}

package com.diploma.web.app.controller;

import com.diploma.web.app.dto.auth.Session;
import com.diploma.web.app.service.api.UserSessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nonnull;

import static com.diploma.web.app.utils.AuthUtils.getCurrentUserToken;

@RestController
public class UserSessionController {

    @Nonnull
    private final UserSessionService userSessionService;

    @Autowired
    public UserSessionController(@Nonnull UserSessionService userSessionService) {
        this.userSessionService = userSessionService;
    }

    @PostMapping("/login")
    public Session login(@Nonnull @RequestParam String email,
                         @Nonnull @RequestParam char[] password) {
        return userSessionService.createSession(email, password);
    }

    @PostMapping("/logout")
    public void logout() {
        userSessionService.deleteSession(getCurrentUserToken());
    }

}

package com.diploma.web.app.controller;

import com.diploma.web.app.dto.user.RegisterUser;
import com.diploma.web.app.dto.user.UpdateUser;
import com.diploma.web.app.dto.user.User;
import com.diploma.web.app.service.api.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Nonnull;

import static com.diploma.web.app.utils.AuthUtils.getCurrentUser;
import static com.diploma.web.app.utils.AuthUtils.getCurrentUserId;

@RestController
@RequestMapping("/user/{role}")
public class UserController {

    @Nonnull
    private final UserService userService;

    @Autowired
    public UserController(@Nonnull UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public User getUser(@PathVariable String role) {
        return userService.loadUserByUserId(getCurrentUserId());
    }

    @PostMapping
    public boolean createUser(@PathVariable String role,
                              @RequestBody RegisterUser user) {
        userService.createUser(user);
        return true;
    }

    @PutMapping
    public User updateUser(@PathVariable String role,
                           @RequestBody UpdateUser user) {
        User currentUser = getCurrentUser();
        return userService.updateUser(currentUser.getId(), currentUser.getEmail(), user);
    }

}

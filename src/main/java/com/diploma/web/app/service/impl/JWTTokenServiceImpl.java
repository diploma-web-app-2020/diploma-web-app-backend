package com.diploma.web.app.service.impl;

import com.diploma.web.app.dto.user.User;
import com.diploma.web.app.service.api.JWTTokenService;
import com.diploma.web.app.utils.exception.UnauthorizedException;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.Jwts;

import javax.annotation.Nonnull;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Map;

public class JWTTokenServiceImpl implements JWTTokenService {

    @Nonnull
    private final PrivateKey privateKey;

    public JWTTokenServiceImpl(@Nonnull byte[] privateKey) {
        try {
            this.privateKey = KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(privateKey));
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public String buildJWTToken(@Nonnull User user,
                                @Nonnull Long expiration) {
        Preconditions.checkNotNull(user);
        return Jwts
                .builder()
                .setClaims(ImmutableMap.of(
                        "userId", Preconditions.checkNotNull(user.getId()),
                        "email", Preconditions.checkNotNull(user.getEmail()),
                        "role", Preconditions.checkNotNull(user.getRole()).toString(),
                        "data", Preconditions.checkNotNull(user.getData()),
                        "expiration", Preconditions.checkNotNull(expiration)
                ))
                .signWith(privateKey)
                .compact();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public Jwt readJWTToken(@Nonnull String jwt) {
        Jwt jwtToken = Jwts
                .parserBuilder()
                .setSigningKey(privateKey)
                .build()
                .parse(jwt);
        Map<String, Object> body = (Map<String, Object>) jwtToken.getBody();
        if (body.size() != 5) {
            throw new UnauthorizedException();
        }
        checkValueNotNull(body, "userId");
        checkValueNotNull(body, "email");
        checkValueNotNull(body, "role");
        checkValueNotNull(body, "data");
        checkValueNotNull(body, "expiration");
        return jwtToken;
    }

    private void checkValueNotNull(@Nonnull Map<String, Object> body,
                                   @Nonnull String key) {
        if (body.get(key) == null) {
            throw new UnauthorizedException();
        }
    }

}

package com.diploma.web.app.service.impl;

import com.diploma.web.app.dao.api.UserSessionDao;
import com.diploma.web.app.dto.auth.Session;
import com.diploma.web.app.dto.user.User;
import com.diploma.web.app.service.api.JWTTokenService;
import com.diploma.web.app.service.api.UserService;
import com.diploma.web.app.service.api.UserSessionService;
import com.diploma.web.app.utils.exception.UnauthorizedException;
import com.diploma.web.app.utils.exception.UserNotFoundException;
import com.google.common.base.Preconditions;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import javax.annotation.Nonnull;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static com.diploma.web.app.utils.PasswordUtils.hashPassword;

public class UserSessionServiceImpl implements UserSessionService {

    @Nonnull
    private final UserService userService;
    @Nonnull
    private final UserSessionDao userSessionDao;
    @Nonnull
    private final JWTTokenService jwtTokenService;
    @Nonnull
    private final Long sessionValidityInMinutes;
    @Nonnull
    private final LoadingCache<String, Session> sessionCache;

    public UserSessionServiceImpl(@Nonnull UserService userService,
                                  @Nonnull UserSessionDao userSessionDao,
                                  @Nonnull JWTTokenService jwtTokenService,
                                  @Nonnull Long sessionValidityInMinutes,
                                  @Nonnull Long tokenCacheValidityInSeconds,
                                  @Nonnull Long tokenRemovalDelayInMinutes) {
        this.userService = Preconditions.checkNotNull(userService);
        this.userSessionDao = Preconditions.checkNotNull(userSessionDao);
        this.jwtTokenService = Preconditions.checkNotNull(jwtTokenService);
        this.sessionValidityInMinutes = Preconditions.checkNotNull(sessionValidityInMinutes);
        //noinspection ConstantConditions
        this.sessionCache = CacheBuilder
                .newBuilder()
                .expireAfterWrite(tokenCacheValidityInSeconds, TimeUnit.SECONDS)
                .build(CacheLoader.from(token -> userSessionDao
                        .loadSession(Preconditions.checkNotNull(token, "Token shouldn't be null"))
                        .orElseThrow(UnauthorizedException::new)));
        Executors.newScheduledThreadPool(5)
                .scheduleAtFixedRate(
                        this::removeExpiredTokens,
                        10,
                        tokenRemovalDelayInMinutes,
                        TimeUnit.MINUTES
                );
    }

    @Override
    public void checkSession(@Nonnull String token) {
        if (isSessionInvalid(sessionCache.getUnchecked(token))) {
            userSessionDao.deleteSession(token);
            sessionCache.invalidate(token);
            throw new UnauthorizedException();
        }
    }

    @Override
    public Session createSession(@Nonnull String email,
                                 @Nonnull char[] password) {
        Preconditions.checkNotNull(email);
        Preconditions.checkNotNull(password);
        User user;
        try {
            user = userService.loadUserByEmail(email);
        } catch (UserNotFoundException e) {
            throw new UnauthorizedException();
        }
        if (!hashPassword(password, user.getSalt()).equals(user.getHashedPassword())) {
            throw new UnauthorizedException();
        }
        Long expiration = Instant.now().toEpochMilli() + sessionValidityInMinutes * 60 * 1000;
        String token = jwtTokenService.buildJWTToken(user, expiration);
        Session session = new Session(token, expiration, user.getId());
        userSessionDao.createSession(session);
        sessionCache.put(token, session);
        return session;
    }

    @Override
    public void deleteSession(@Nonnull String token) {
        userSessionDao.deleteSession(token);
        sessionCache.invalidate(token);
    }

    @Override
    public void deleteUserSessions(@Nonnull Long userId) {
        List<String> deletedTokens = userSessionDao.deleteUserSessions(userId);
        sessionCache.invalidateAll(deletedTokens);
    }

    private void removeExpiredTokens() {
        userSessionDao
                .loadAllSessions()
                .stream()
                .filter(this::isSessionInvalid)
                .map(Session::getToken)
                .forEach(token -> {
                    userSessionDao.deleteSession(token);
                    sessionCache.invalidate(token);
                });
    }

    private boolean isSessionInvalid(@Nonnull Session session) {
        return session.getExpiration() <= Instant.now().toEpochMilli();
    }

}

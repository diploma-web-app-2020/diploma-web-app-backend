package com.diploma.web.app.service.impl;

import com.diploma.web.app.dao.api.UserDao;
import com.diploma.web.app.dto.user.RegisterUser;
import com.diploma.web.app.dto.user.UpdateUser;
import com.diploma.web.app.dto.user.User;
import com.diploma.web.app.service.api.UserDataValidator;
import com.diploma.web.app.service.api.UserService;
import com.diploma.web.app.utils.exception.UserNotFoundException;
import com.google.common.base.Preconditions;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.util.concurrent.UncheckedExecutionException;

import javax.annotation.Nonnull;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.diploma.web.app.utils.PasswordUtils.hashPassword;

public class UserServiceImpl implements UserService {

    private static final String ALPHA_NUMERICAL_STRING_DATA = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            + "abcdefghijklmnopqrstuvwxyz"
            + "0123456789";

    @Nonnull
    private final UserDao userDao;
    @Nonnull
    private final UserDataValidator userDataValidator;
    @Nonnull
    private final LoadingCache<Long, User> usersByIdCache;
    @Nonnull
    private final LoadingCache<String, User> usersByEmailCache;

    @SuppressWarnings("ConstantConditions")
    public UserServiceImpl(@Nonnull UserDao userDao,
                           @Nonnull UserDataValidator userDataValidator) {
        this.userDao = Preconditions.checkNotNull(userDao);
        this.userDataValidator = Preconditions.checkNotNull(userDataValidator);
        this.usersByIdCache = CacheBuilder
                .newBuilder()
                .build(CacheLoader.from(userId -> userDao
                        .loadUserById(Preconditions.checkNotNull(userId, "UserId shouldn't be null"))
                        .orElseThrow(() -> new UserNotFoundException(userId))));
        this.usersByEmailCache = CacheBuilder
                .newBuilder()
                .build(CacheLoader.from(email -> userDao
                        .loadUserByEmail(Preconditions.checkNotNull(email, "Email shouldn't be null"))
                        .orElseThrow(() -> new UserNotFoundException(email))));
    }

    @Nonnull
    @Override
    public User loadUserByUserId(@Nonnull Long userId) {
        try {
            return usersByIdCache.getUnchecked(userId);
        } catch (UncheckedExecutionException e) {
            if (e.getCause() instanceof RuntimeException) {
                throw (RuntimeException) e.getCause();
            }
            throw e;
        }
    }

    @Nonnull
    @Override
    public User loadUserByEmail(@Nonnull String email) {
        try {
            return usersByEmailCache.getUnchecked(email);
        } catch (UncheckedExecutionException e) {
            if (e.getCause() instanceof RuntimeException) {
                throw (RuntimeException) e.getCause();
            }
            throw e;
        }
    }

    @Override
    public boolean isExistingUserEmail(@Nonnull String email) {
        try {
            usersByEmailCache.getUnchecked(email);
        } catch (UncheckedExecutionException e) {
            if (e.getCause() instanceof UserNotFoundException) {
                return false;
            }
            throw e;
        }
        return true;
    }

    @Nonnull
    @Override
    public User createUser(@Nonnull RegisterUser registerUser) {
        userDataValidator.validatePassword(registerUser.getPassword());
        userDataValidator.validateEmail(registerUser.getEmail());
        String salt = generateRandomSalt();
        User newUser = new User(
                null,
                registerUser.getEmail(),
                registerUser.getRole(),
                registerUser.getData(),
                false,
                hashPassword(registerUser.getPassword(), salt),
                salt
        );
        Long userId = userDao.createUser(newUser);
        newUser.setId(userId);
        usersByIdCache.put(newUser.getId(), newUser);
        usersByEmailCache.put(newUser.getEmail(), newUser);
        return newUser;
    }

    @Nonnull
    @Override
    public User updateUser(@Nonnull Long userId,
                           @Nonnull String email,
                           @Nonnull UpdateUser updateUser) {
        userDao.updateUserData(userId, updateUser.getData());
        User updatedUser = userDao
                .loadUserById(userId)
                .orElseThrow(() -> new UserNotFoundException(userId));
        usersByIdCache.put(userId, updatedUser);
        usersByEmailCache.put(email, updatedUser);
        return updatedUser;
    }

    @Nonnull
    private String generateRandomSalt() {
        return IntStream
                .range(0, 32)
                .mapToObj(i -> ALPHA_NUMERICAL_STRING_DATA
                        .charAt((int) (Math.random() * ALPHA_NUMERICAL_STRING_DATA.length())))
                .map(String::valueOf)
                .collect(Collectors.joining(""));
    }

}

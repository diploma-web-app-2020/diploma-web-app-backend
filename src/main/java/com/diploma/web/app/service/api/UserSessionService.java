package com.diploma.web.app.service.api;

import com.diploma.web.app.dto.auth.Session;
import com.diploma.web.app.utils.exception.UnauthorizedException;

import javax.annotation.Nonnull;

public interface UserSessionService {

    /**
     * Checks if the given token belongs to a valid session. If the session is invalid, deletes it.
     *
     * @param token to check.
     * @throws UnauthorizedException if the given token doesn't belong to any session or the session is expired
     */
    void checkSession(@Nonnull String token);

    /**
     * Creates a new session for the user with the given email and password.
     *
     * @param email    of the requested user
     * @param password of the requested user
     * @return the newly created session.
     * @throws UnauthorizedException if a user with the given email/password combination doesn't exist
     */
    Session createSession(@Nonnull String email,
                          @Nonnull char[] password);

    /**
     * Deletes session with the given token.
     *
     * @param token of the requested session
     */
    void deleteSession(@Nonnull String token);

    /**
     * Deletes all session of the user with the given id
     *
     * @param userId of the requested user sessions
     */
    void deleteUserSessions(@Nonnull Long userId);

}

package com.diploma.web.app.service.api;

import com.diploma.web.app.utils.exception.InvalidUserDataException;

public interface UserDataValidator {

    /**
     * Makes sure that the given password conforms application password policy.
     *
     * @param password to validate
     * @throws InvalidUserDataException if password is invalid
     */
    void validatePassword(char[] password);

    /**
     * Makes sure that the given email conforms application email policy.
     *
     * @param email to validate
     * @throws InvalidUserDataException if email is invalid
     */
    void validateEmail(String email);

}

package com.diploma.web.app.service.api;

import com.diploma.web.app.dto.user.User;
import io.jsonwebtoken.Jwt;

import javax.annotation.Nonnull;

public interface JWTTokenService {

    String buildJWTToken(@Nonnull User user,
                         @Nonnull Long expiration);

    @SuppressWarnings("rawtypes")
    Jwt readJWTToken(@Nonnull String jwt);

}

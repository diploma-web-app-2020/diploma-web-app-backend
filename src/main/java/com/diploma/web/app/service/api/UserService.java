package com.diploma.web.app.service.api;

import com.diploma.web.app.dto.user.RegisterUser;
import com.diploma.web.app.dto.user.UpdateUser;
import com.diploma.web.app.dto.user.User;
import com.diploma.web.app.utils.exception.UserNotFoundException;

import javax.annotation.Nonnull;

public interface UserService {

    /**
     * Loads user with the given id.
     *
     * @param userId of the requested user
     * @return {@link User} with the given id.
     * @throws UserNotFoundException if a user with the given id was not found
     */
    @Nonnull
    User loadUserByUserId(@Nonnull Long userId);

    /**
     * Loads user with the given email.
     *
     * @param email of the requested user
     * @return {@link User} with the given email.
     * @throws UserNotFoundException if a user with the given email was not found
     */
    @Nonnull
    User loadUserByEmail(@Nonnull String email);

    /**
     * Checks if a user with the given email exists.
     *
     * @param email of the requested user
     * @return {@code true} if user exists and {@code false} otherwise.
     */
    boolean isExistingUserEmail(@Nonnull String email);

    /**
     * Creates a new user with the given data.
     *
     * @param user to create
     * @return the created user.
     */
    @Nonnull
    User createUser(@Nonnull RegisterUser user);

    /**
     * Updates user with the given data.
     *
     * @param userId of the requested user
     * @param email  of the requested user
     * @param user   data to update the user with
     * @return the updated user.
     * @throws UserNotFoundException if a user with the given id was not found
     */
    @Nonnull
    User updateUser(@Nonnull Long userId,
                    @Nonnull String email,
                    @Nonnull UpdateUser user);

}

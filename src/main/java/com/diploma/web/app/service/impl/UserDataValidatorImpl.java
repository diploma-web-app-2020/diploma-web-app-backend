package com.diploma.web.app.service.impl;

import com.diploma.web.app.dao.api.UserDao;
import com.diploma.web.app.service.api.UserDataValidator;
import com.diploma.web.app.utils.exception.InvalidUserDataException;

import javax.annotation.Nonnull;

public class UserDataValidatorImpl implements UserDataValidator {

    private final static String EMAIL_PATTERN = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    private final static String LOWER_AND_UPPER_CASE_LETTERS_PATTERN = ".*[a-z]+.*[A-Z]+.*|.*[A-Z]+.*[a-z]+.*";
    private final static String NUMBER_PATTERN = ".*[0-9]+.*";
    private final static String SPECIAL_CHARACTERS_PATTERN = ".*[^A-Za-z0-9]+.*";

    private final int minPasswordLength;
    private final int maxPasswordLength;
    private final boolean lowerAndUpperCaseCheck;
    private final boolean numberCheck;
    private final boolean specialCharacterCheck;
    @Nonnull
    private final UserDao userDao;

    public UserDataValidatorImpl(int minPasswordLength,
                                 int maxPasswordLength,
                                 boolean lowerAndUpperCaseCheck,
                                 boolean numberCheck,
                                 boolean specialCharacterCheck,
                                 @Nonnull UserDao userDao) {
        this.minPasswordLength = minPasswordLength;
        this.maxPasswordLength = maxPasswordLength;
        this.lowerAndUpperCaseCheck = lowerAndUpperCaseCheck;
        this.numberCheck = numberCheck;
        this.specialCharacterCheck = specialCharacterCheck;
        this.userDao = userDao;
    }

    @Override
    public void validatePassword(char[] password) {
        if (password.length < minPasswordLength) {
            throw new InvalidUserDataException("Password is too short");
        }
        if (password.length > maxPasswordLength) {
            throw new InvalidUserDataException("Password is too long");
        }
        if (lowerAndUpperCaseCheck && !String.valueOf(password).matches(LOWER_AND_UPPER_CASE_LETTERS_PATTERN)) {
            throw new InvalidUserDataException("Password should have at least one lower and one upper case letter");
        }
        if (numberCheck && !String.valueOf(password).matches(NUMBER_PATTERN)) {
            throw new InvalidUserDataException("Password should have at least one number");
        }
        if (specialCharacterCheck && !String.valueOf(password).matches(SPECIAL_CHARACTERS_PATTERN)) {
            throw new InvalidUserDataException("Password should have at least one special character");
        }
    }

    @Override
    public void validateEmail(String email) {
        if (!email.matches(EMAIL_PATTERN)) {
            throw new InvalidUserDataException("Email format is incorrect");
        }
        if (userDao.isExistingUserEmail(email)) {
            throw new InvalidUserDataException("Email is already registered");
        }
    }

}

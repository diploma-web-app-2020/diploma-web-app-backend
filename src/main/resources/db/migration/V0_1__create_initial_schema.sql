create type role as enum ('USER', 'ORGANIZATION');

create type period as enum ('START', 'MIDDLE', 'END');

create type notification_type as enum ('REMINDER', 'SUBSCRIPTION', 'PAYMENT_FAILED');

create table users
(
    id              bigserial primary key,
    email           varchar(320) unique not null,
    role            role                not null,
    is_blocked      boolean             not null,
    data            json                not null,
    hashed_password varchar(256)        not null,
    salt            varchar(256)        not null
);

create table user_sessions
(
    user_id    bigint references users (id),
    token      varchar not null,
    expiration timestamp not null
);

create table recovery_tokens
(
    user_id    bigint references users (id),
    token      varchar   not null,
    expiration timestamp not null,
    checked    boolean   not null default false
);

create table email_confirmation_tokens
(
    user_id          bigint references users (id),
    email            varchar(320) references users (email),
    token            varchar(256) not null,
    used_first_login boolean      not null default false
);

create table goals
(
    id                bigserial primary key,
    organization_user bigint references users (id),
    name              varchar(100) not null,
    amount            float        not null default 0,
    active            boolean      not null default true,
    description       varchar,
    thumbnail         varchar
);

create table images
(
    goal_id     bigint references goals (id),
    title       varchar(256) not null,
    path        varchar      not null,
    isThumbnail boolean      not null default false
);

create table subscriptions
(
    user_id     bigint references users (id),
    goal_id     bigint references goals (id),
    draw_amount float not null
);

create table donations
(
    user_id bigint references users (id),
    goal_id bigint references goals (id),
    time    timestamp not null,
    amount  float     not null
);

create table notifications
(
    notification_id bigserial primary key,
    user_id         bigint references users (id),
    goal_id         bigint references goals (id),
    is_new          boolean           not null default true,
    type            notification_type not null,
    data            json
);

create table reminders
(
    user_id bigint references users (id),
    goal_id bigint references goals (id),
    period  period not null
);